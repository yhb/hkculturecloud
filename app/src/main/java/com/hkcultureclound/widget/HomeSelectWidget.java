package com.hkcultureclound.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.hkculturecloud.R;

/**
 * Created by yanghongbing on 17/7/9.
 */

public class HomeSelectWidget extends FrameLayout implements View.OnClickListener {

    private CheckedTextView homeCT;
    private CheckedTextView findCT;
    private ImageView scanCT;
    private CheckedTextView footCT;
    private CheckedTextView myCT;

    private CheckedTextView[] buttonList;

    public static final int CHECKED_HOME = 0;
    public static final int CHECKED_FIND = 1;
    public static final int CHECKED_FOOT = 2;
    public static final int CHECKED_MY = 3;
    public static final int CHECKED_PHOTO = 4;

    private OnSelectedListener onSelectedListener;

    public HomeSelectWidget(@NonNull Context context) {
        this(context, null);
    }

    public HomeSelectWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.home_selector, this);
        homeCT = (CheckedTextView) findViewById(R.id.home);
        findCT = (CheckedTextView) findViewById(R.id.find);
        scanCT = (ImageView) findViewById(R.id.photo);
        footCT = (CheckedTextView) findViewById(R.id.foot);
        myCT = (CheckedTextView) findViewById(R.id.my);
        buttonList = new CheckedTextView[] {homeCT, findCT, footCT, myCT};
        homeCT.setOnClickListener(this);
        findCT.setOnClickListener(this);
        scanCT.setOnClickListener(this);
        footCT.setOnClickListener(this);
        myCT.setOnClickListener(this);
    }

    public void select(int which) {
        if (which == CHECKED_PHOTO) {
            scanCT.performClick();
        } else {
            if (which >= 0 && which < buttonList.length) {
                buttonList[which].performClick();
            }
        }
    }

    @Override
    public void onClick(View v) {
        int checked = CHECKED_HOME;
        if (v == scanCT) {
            checked = CHECKED_PHOTO;
        } else {
            for (int i = 0; i < buttonList.length; i++) {
                CheckedTextView view = buttonList[i];
                view.setChecked(view == v);
                if (view == v) {
                    checked = i;
                }
            }
        }
        if (onSelectedListener != null) {
            onSelectedListener.onSelect(checked);
        }
    }

    public void setOnSelectedListener(OnSelectedListener onSelectedListener) {
        this.onSelectedListener = onSelectedListener;
    }

    public interface OnSelectedListener {
        void onSelect(int whitch);
    }
}
