package com.hkcultureclound.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hkculturecloud.R;


/**
 * @author yanghongbing615
 */
public class PageIndicator extends LinearLayout {
	private int num;
	private int selectPos = 0; //当前选中位置
	
	private int radioOnId = R.mipmap.btn_radio_on;
	private int radioOffId = R.mipmap.btn_radio_off;
	/**
	 * @param context
	 */
	public PageIndicator(Context context) {
		super(context);
		init();
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public PageIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		setOrientation(HORIZONTAL);
		setGravity(Gravity.CENTER);
	}

	private void updateLayout() {
		removeAllViews();
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.leftMargin = 5;
		layoutParams.rightMargin = 5;
		for(int i = 0 ; i < num; i++) {
			ImageView imageView = new ImageView(getContext());
			imageView.setLayoutParams(layoutParams);
			imageView.setBackgroundResource(radioOffId);
			addView(imageView);
		}
		getChildAt(selectPos).setBackgroundResource(radioOnId);
	}

	public void setNum(int num){
		this.num = num;
		updateLayout();
	}

	public void setIndicatorImage(int onImageId, int offImageId) {
		this.radioOnId = onImageId;
		this.radioOffId = offImageId;
	}
	
	public void select(int pos) {
		getChildAt(selectPos).setBackgroundResource(radioOffId);
		getChildAt(pos).setBackgroundResource(radioOnId);
		selectPos = pos;
	}
}
