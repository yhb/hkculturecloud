package com.hkcultureclound.widget;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by yanghongbing on 17/7/11.
 */

public class AppUtils {

    public static int[] getScreenSize(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = activity.getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        int[] size = new int[2];
        size[0] = display.getWidth();
        size[1] = display.getHeight();
        return size;
    }
}
