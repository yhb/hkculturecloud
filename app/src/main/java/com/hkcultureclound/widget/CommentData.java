package com.hkcultureclound.widget;

/**
 * Created by yanghongbing on 17/7/11.
 */

public class CommentData {

    public CommentData(String content, String user, String time) {
        this.content = content;
        this.user = user;
        this.time = time;
    }

    public String content;
    public String user;
    public String time;
}
