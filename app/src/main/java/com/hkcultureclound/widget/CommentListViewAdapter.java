package com.hkcultureclound.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hkculturecloud.R;

import java.util.List;

/**
 * Created by yanghongbing on 17/7/11.
 */

public class CommentListViewAdapter extends BaseAdapter {

    private Context context;
    private List<CommentData> data;

    public CommentListViewAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<CommentData> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.comment_item, null);
            viewHolder = new ViewHolder();
            viewHolder.contentTV = (TextView) convertView.findViewById(R.id.comment_content);
            viewHolder.userTV = (TextView) convertView.findViewById(R.id.comment_user);
            viewHolder.timeTV = (TextView) convertView.findViewById(R.id.comment_time);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CommentData item = data.get(position);
        viewHolder.contentTV.setText(item.content);
        viewHolder.userTV.setText(item.user);
        viewHolder.timeTV.setText(item.time);
        return convertView;
    }

    class ViewHolder {
        public TextView contentTV;
        public TextView userTV;
        public TextView timeTV;
    }
}
