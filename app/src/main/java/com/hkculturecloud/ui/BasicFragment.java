package com.hkculturecloud.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hkculturecloud.R;

/**
 * Created by yanghongbing on 17/7/11.
 */

public class BasicFragment extends Fragment {

    private View titleView;
    private ImageView titleLeft;
    private TextView titleTV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.body_layout, null);
        titleTV = (TextView) view.findViewById(R.id.title_center);
        titleView = view.findViewById(R.id.title);
        titleLeft = (ImageView) view.findViewById(R.id.title_left);
        return view;
    }

    public void setTitle(String title) {
        titleTV.setText(title);
    }

    public void showTitle(boolean show) {
        if (show) {
            titleView.setVisibility(View.VISIBLE);
        } else {
            titleView.setVisibility(View.GONE);
        }
    }

    public void showTitleLeft(boolean show) {
        if (show) {
            titleLeft.setVisibility(View.VISIBLE);
        } else {
            titleLeft.setVisibility(View.INVISIBLE);
        }
    }
}
