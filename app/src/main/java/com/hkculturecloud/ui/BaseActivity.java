package com.hkculturecloud.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.hkculturecloud.R;

/**
 * Created by yanghongbing on 17/7/9.
 */

public class BaseActivity extends FragmentActivity implements View.OnClickListener {

    private View titleView;
    private ImageView titleLeft;
    private TextView titleTV;

    private ViewGroup bodyVG;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.body_layout);
        titleTV = (TextView) findViewById(R.id.title_center);
        titleView = findViewById(R.id.title);
        titleLeft = (ImageView) findViewById(R.id.title_left);
        titleLeft.setOnClickListener(this);
        bodyVG = (ViewGroup) findViewById(R.id.body);
    }

    @Override
    public void setContentView(int layoutResID) {
        bodyVG.removeAllViews();
        getLayoutInflater().inflate(layoutResID, bodyVG);
    }

    public void setTitle(String title) {
        titleTV.setText(title);
    }

    public void showTitle(boolean show) {
        if (show) {
            titleView.setVisibility(View.VISIBLE);
        } else {
            titleView.setVisibility(View.GONE);
        }
    }

    public void showTitleLeft(boolean show) {
        if (show) {
            titleLeft.setVisibility(View.VISIBLE);
        } else {
            titleLeft.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == titleLeft) {
            finish();
        }
    }
}
