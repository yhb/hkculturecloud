package com.hkculturecloud.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.hkculturecloud.R;
import com.hkcultureclound.widget.CommentData;
import com.hkcultureclound.widget.CommentListViewAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by yanghongbing on 17/7/11.
 */

public class SceneActivity extends BaseActivity implements View.OnClickListener {

    private ListView commentListView;
    private CommentListViewAdapter adapter;

    private EditText inputComment;
    private Button publishComment;

    private List<CommentData> commentDataList;

    private ProgressDialog mDialog;
    private Handler handler =  new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scene_activity);
        setTitle("历史景点");
        commentListView = (ListView) findViewById(R.id.comment_list);
        adapter = new CommentListViewAdapter(this);
        getData();
        adapter.setData(commentDataList);
        commentListView.setAdapter(adapter);

        inputComment = (EditText) findViewById(R.id.comment_input);
        publishComment = (Button) findViewById(R.id.comment_publish);
        publishComment.setOnClickListener(this);

    }

    private void getData() {
        commentDataList = new ArrayList<>();
        commentDataList.add(new CommentData("这里的风景很不错，很喜欢这里.", "yhb", "2017-03-08 16:23:21"));
        commentDataList.add(new CommentData("很喜欢这里的感觉，很富有历史感和科技感", "qjn", "2017-05-06 12:11:43"));
    }

    @Override
    public void onClick(View v) {
        if (v == publishComment) {
            final String content = inputComment.getText().toString();
            if (TextUtils.isEmpty(content)) {
                Toast.makeText(this, "请输入评论内容", Toast.LENGTH_LONG).show();
                return;
            }
            mDialog = ProgressDialog.show(this, null, "加载中");
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String user = "张三";
                    String time = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
                    CommentData data = new CommentData(content, user, time);
                    commentDataList.add(data);
                    adapter.notifyDataSetChanged();
                    inputComment.setText("");
                    mDialog.dismiss();
                }
            }, 500);

        }
    }
}
