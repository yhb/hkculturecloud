package com.hkculturecloud.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.hkculturecloud.R;

import java.io.IOException;
import java.security.Policy;

/**
 * Created by yanghongbing on 17/7/11.
 */

public class CaptureActivity extends BaseActivity implements SurfaceHolder.Callback, Camera.PreviewCallback, View.OnClickListener, Camera.ShutterCallback, Camera.PictureCallback {

    private SurfaceView surfaceView;
    private ImageView captureBtn;
    private SurfaceHolder surfaceHolder;
    private Camera mCamera;

    private ViewGroup showLayout;
    private ImageView imageView;
    private Button okBtn;
    private Button cancelBtn;

    private ProgressDialog mDialog;

    private Handler handler = new Handler();

    private boolean isCapture = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capture_activity);
        showTitle(false);
        surfaceView = (SurfaceView) findViewById(R.id.surface_view);
        captureBtn = (ImageView) findViewById(R.id.capture_button);
        captureBtn.setOnClickListener(this);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);

        showLayout = (ViewGroup) findViewById(R.id.show_image_view);
        imageView = (ImageView) showLayout.findViewById(R.id.show_image);
        okBtn = (Button) showLayout.findViewById(R.id.show_ok);
        cancelBtn = (Button)showLayout.findViewById(R.id.show_cancel);
        okBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        showLayout.setVisibility(View.GONE);



    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (mCamera == null) {
            mCamera = Camera.open();
        } else {
            try {
                mCamera.reconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        initCamera();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Camera.Parameters params = mCamera.getParameters();
        params.setPreviewSize(width, height);
//        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//            params.set("rotation", 0);
//            params.set("orientation", "portrait");
//        } else {
//            params.set("rotation", 90);
//            params.set("orientation", "landscape");
//        }
        byte[] buf = new byte[width * height * 3 /2];
        mCamera.addCallbackBuffer(buf);
        mCamera.setDisplayOrientation(90);
        mCamera.setPreviewCallback(this);

        try {
            mCamera.setPreviewDisplay(surfaceHolder);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    private void initCamera() {
        Camera.Parameters pams = mCamera.getParameters();
        pams.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
        pams.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        pams.setPreviewFormat(ImageFormat.JPEG);
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

    }

    @Override
    public void onClick(View v) {
        if (v == captureBtn) {
            mCamera.takePicture(this, new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    if (data != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                        Canvas canvas = surfaceHolder.lockCanvas();
                        canvas.drawBitmap(bitmap, 0, 0, null);
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }, new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    if (data != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//                        mDialog = ProgressDialog.show(CaptureActivity.this, null, "正在加载");
                        showLayout.setVisibility(View.VISIBLE);
                        imageView.setImageBitmap(bitmap);
                    }
                }
            });
        } else if (v == okBtn) {
            mDialog = ProgressDialog.show(CaptureActivity.this, null, "请稍后...");
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDialog.dismiss();
                    showLayout.setVisibility(View.GONE);
                    Intent intent = new Intent(CaptureActivity.this, SceneActivity.class);
                    startActivity(intent);
                }
            }, 1000);
        } else if (v == cancelBtn){
            imageView.setImageBitmap(null);
            showLayout.setVisibility(View.GONE);
            mCamera.startPreview();
        }
    }

    @Override
    public void onShutter() {

    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

    }
}
