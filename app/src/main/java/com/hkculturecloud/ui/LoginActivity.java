package com.hkculturecloud.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hkculturecloud.R;

/**
 * Created by yanghongbing on 17/7/9.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText accountET;
    private EditText passwordET;
    private Button submitBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        showTitle(false);
        initViews();
    }

    private void initViews() {
        accountET = (EditText) findViewById(R.id.input_account);
        passwordET = (EditText) findViewById(R.id.input_password);
        submitBtn = (Button) findViewById(R.id.btn_login);
        submitBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == submitBtn) {
            onSubmit();
        }
    }

    private void onSubmit() {
        if (TextUtils.isEmpty(accountET.getText())) {
            Toast.makeText(this, "请输入账号!", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(passwordET.getText())) {
            Toast.makeText(this, "请输入密码!", Toast.LENGTH_LONG).show();
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(this,  null, "正在登录...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }).start();
    }
}
