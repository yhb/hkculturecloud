package com.hkculturecloud.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hkculturecloud.R;
import com.hkcultureclound.widget.PageIndicator;
import com.yhb.androidcore.cache.CacheManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yanghongbing on 17/7/12.
 */

public class GuideActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;
    private PageIndicator indicator;
    private CacheManager cacheManager;

    private final int[] guideImages = {
            R.mipmap.guide_1, R.mipmap.guide_2, R.mipmap.guide_3
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide_activity);
        showTitle(false);
        cacheManager = new CacheManager();

        viewPager = (ViewPager) findViewById(R.id.guide_viewpager);
        indicator = (PageIndicator) findViewById(R.id.guide_indicator);
        viewPager.setOnPageChangeListener(this);
        List<View> viewList = getGuideViewList();
        viewPager.setAdapter(new GuidePagerAdapter(viewList));
        indicator.setNum(viewList.size());
        indicator.select(0);
    }

    private List<View> getGuideViewList() {
        List<View> viewList = new ArrayList<View>();
        for (int i = 0; i < guideImages.length; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setImageResource(guideImages[i]);
            viewList.add(imageView);
            if (i == guideImages.length - 1) {
                imageView.setTag("lastPage");
                imageView.setOnClickListener(this);
            }
        }
        return viewList;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if ("lastPage".equals(v.getTag())) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            cacheManager.setCache(CacheManager.MODE_PREFERENCE, "isRuned", true);
            finish();
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        indicator.select(i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    private class GuidePagerAdapter extends PagerAdapter {

        private List<View> viewList;

        public GuidePagerAdapter(List<View> viewList) {
            this.viewList = viewList;
        }

        @Override
        public int getCount() {
            if (viewList == null)
                return 0;
            return viewList.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View imageView = viewList.get(position);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            container.addView(imageView, lp);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(viewList.get(position));
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }
    }
}
