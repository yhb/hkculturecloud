package com.hkculturecloud.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.hkculturecloud.R;
import com.hkcultureclound.widget.HomeSelectWidget;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yanghongbing on 17/7/9.
 */

public class HomeActivity extends BaseActivity implements HomeSelectWidget.OnSelectedListener {

    private HomeSelectWidget homeSelectWidget;

    private List<BasicFragment> fragments;

    private HomeFragment homeFragment;
    private FindFragment findFragment;
    private FootFragment footFragment;
    private MyFragment myFragment;

    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        showTitle(false);
        homeSelectWidget = (HomeSelectWidget) findViewById(R.id.home_selector);
        homeSelectWidget.setOnSelectedListener(this);
        fragments = new ArrayList<>();
        homeFragment = new HomeFragment();
        findFragment = new FindFragment();
        footFragment = new FootFragment();
        myFragment = new MyFragment();
        fragments.add(homeFragment);
        fragments.add(findFragment);
        fragments.add(footFragment);
        fragments.add(myFragment);

        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        for (int i = 0; i < fragments.size(); i++) {
            transaction.add(R.id.home_content, fragments.get(i));
            transaction.hide(fragments.get(i));
        }
        transaction.commit();

        homeSelectWidget.select(HomeSelectWidget.CHECKED_HOME);
    }

    @Override
    public void onSelect(int whitch) {
        if (whitch == HomeSelectWidget.CHECKED_PHOTO) {
            Intent intent = new Intent(this, CaptureActivity.class);
            startActivity(intent);
        } else {
            Fragment fragment = fragments.get(whitch);
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            for (Fragment f : fragments) {
                if (f == fragment) {
                    transaction.show(f);
                } else {
                    transaction.hide(f);
                }
            }
            transaction.commit();
        }
    }
}
