package com.hkculturecloud.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.hkculturecloud.R;
import com.yhb.androidcore.cache.CacheManager;

/**
 * Created by yanghongbing on 17/7/9.
 */

public class SplashActivity extends BaseActivity {

    private Handler mHandler;
    private CacheManager cacheManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        showTitle(false);
        cacheManager = new CacheManager();
        final String isRuned = (String) cacheManager.getCache(CacheManager.MODE_PREFERENCE, "isRuned");
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if ("true".equals(isRuned)) {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashActivity.this, GuideActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        }, 1000);
    }
}
