package com.yhb.androidcore.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by yanghongbing on 16/3/3.
 */
public class BaseDBUtils {

    private SQLiteDatabase database;
    private DataBaseHelper dataHelper;

    private static volatile BaseDBUtils instance;

    private BaseDBUtils() {
        dataHelper = new DataBaseHelper();
        database = dataHelper.getWritableDatabase();
    }

    public static BaseDBUtils getInstance() {
        if (instance == null) {
            synchronized (BaseDBUtils.class) {
                if (instance == null) {
                    instance = new BaseDBUtils();
                }
            }
        }
        return instance;
    }

    public SQLiteDatabase getDatabase() {
        return database;
    }
}
