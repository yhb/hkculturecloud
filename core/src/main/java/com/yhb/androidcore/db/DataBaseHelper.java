package com.yhb.androidcore.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.yhb.androidcore.application.BaseApplication;

/**
 * Created by yanghongbing on 16/3/3.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = BaseApplication.getInstance().getPackageName() + ".db";



    public DataBaseHelper() {
        super(BaseApplication.getInstance(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (int i = 0; i < DBTables.CREATE_TABLES.length; i++) {
            sqLiteDatabase.execSQL(DBTables.CREATE_TABLES[i]);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        for (int i = 0; i < DBTables.TABLES.length; i++) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DBTables.TABLES[i]);
        }
        onCreate(sqLiteDatabase);
    }

}
