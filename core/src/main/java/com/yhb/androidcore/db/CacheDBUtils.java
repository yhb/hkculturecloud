package com.yhb.androidcore.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by yanghongbing on 16/3/3.
 */
public class CacheDBUtils {

    private SQLiteDatabase database;

    public CacheDBUtils() {
        database = BaseDBUtils.getInstance().getDatabase();
    }

    public String getCache(String key) {
        String[] columns = new String[]{DBTables.COLUMN_CACHE_VALUE};
        String[] queryArgs = new String[]{key};
        String selection = DBTables.COLUMN_CACHE_KEY + "=?";
        Cursor cursor = database.query(DBTables.TABLE_CACHE, columns, selection, queryArgs, null, null,  null);
        cursor.moveToFirst();
        String value = null;
        if (!cursor.isAfterLast()) {
            value = cursor.getString(cursor.getColumnIndex(DBTables.COLUMN_CACHE_VALUE));
        }
        cursor.close();
        return value;
    }

    public List<String> getAllCacheKeys() {
        String[] columns = new String[] {DBTables.COLUMN_CACHE_KEY};
        Cursor cursor = database.query(DBTables.TABLE_CACHE, columns, null, null, null, null, null);
        cursor.moveToFirst();
        List<String> keyList = new ArrayList<String>();
        while (cursor.moveToNext()) {
            String key = cursor.getString(cursor.getColumnIndex(DBTables.COLUMN_CACHE_KEY));
            keyList.add(key);
        }
        cursor.close();
        return keyList;
    }

    public void updateCache(String key, String value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBTables.COLUMN_CACHE_KEY, key);
        contentValues.put(DBTables.COLUMN_CACHE_VALUE, value);
        String[] columns = new String[] {DBTables.COLUMN_CACHE_KEY};
        String[] values = new String[] {value};
        String where = DBTables.COLUMN_CACHE_KEY + "=?";
        database.update(DBTables.TABLE_CACHE, contentValues, where, values);
    }

    public void deleteCache(String key) {
        String where = DBTables.COLUMN_CACHE_KEY + "=?";
        database.delete(DBTables.TABLE_CACHE, where, new String[]{key});
    }

    public void deleteAllCache() {
        database.delete(DBTables.TABLE_CACHE, null, null);
    }

    public void insertCache(String key, String value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBTables.COLUMN_CACHE_KEY, key);
        contentValues.put(DBTables.COLUMN_CACHE_VALUE, value);
        database.insert(DBTables.TABLE_CACHE, null, contentValues);
    }

    public void insertCacheInBatch(Map<String, String> configMap) {
        if (configMap == null || configMap.isEmpty()) {
            return;
        }
        database.beginTransaction();
        try {
            for (Map.Entry<String, String> configEntry: configMap.entrySet()) {
                String cache = getCache(configEntry.getKey());
                if (cache != null) {
                    updateCache(configEntry.getKey(), configEntry.getValue());
                } else {
                    insertCache(configEntry.getKey(), configEntry.getValue());
                }
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }
}
