package com.yhb.androidcore.db;

/**
 * Created by yanghongbing on 16/3/4.
 */
public class DBTables {

    //配置项表
    public static final String TABLE_CONFIG = "config";
    public static final String COLUMN_CONFIG_NAME = "name";
    public static final String COLUMN_CONFIG_VALUE = "value";

    private static final String CREATE_TABLE_CONFIG = "CREATE TABLE IF NOT EXISTS " + TABLE_CONFIG + " (" +
            COLUMN_CONFIG_NAME + " TEXT primary key, " +
            COLUMN_CONFIG_VALUE + " TEXT)";

    //缓存表
    public static final String TABLE_CACHE = "cache";
    public static final String COLUMN_CACHE_KEY = "key";
    public static final String COLUMN_CACHE_VALUE = "value";

    private static final String CREATE_TABLE_CACHE = "CREATE TABLE IF NOT EXISTS " + TABLE_CACHE + " (" +
            COLUMN_CACHE_KEY + " TEXT primary key, " +
            COLUMN_CACHE_VALUE + " TEXT)";



    public static String[] CREATE_TABLES =
            {CREATE_TABLE_CONFIG, CREATE_TABLE_CACHE};

    public static String[] TABLES = {TABLE_CONFIG, TABLE_CACHE};

}
