package com.yhb.androidcore.network.http;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yanghongbing on 16/12/31.
 */

public class HttpRequest implements IRequest {

    private int requestId;
    private String url;
    private Map<String, String> params;
    private Map<String, String> headers;
    private String method = "GET";

    public HttpRequest(int requestId, String url) {
        this(requestId, url, null);
    }

    public HttpRequest(int requestId, String url, Map<String, String> params) {
        this.requestId = requestId;
        this.url = url;
        this.params = params;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getMethod() {
        return method;
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public int getRequestId() {
        return requestId;
    }

    @Override
    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public void addParam(String key, String value) {
        if (this.params == null) {
            this.params = new HashMap<String, String>();
        }
        this.params.put(key, value);
    }

    @Override
    public void addParams(Map<String, String> params) {
        if (this.params == null) {
            this.params = params;
        } else {
            this.params.putAll(params);
        }
    }

    @Override
    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
