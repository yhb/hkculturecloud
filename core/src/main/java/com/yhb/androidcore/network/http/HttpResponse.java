package com.yhb.androidcore.network.http;

/**
 * Created by yanghongbing on 16/12/31.
 */

public class HttpResponse implements IResponse {

    private int code; //相应码
    private String message; //响应报文
    private long sendRequestAtMillis; //发送请求时间戳
    private long receivedResponseAtMillis; //收到响应时间戳


    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public long getSendRequestAtMillis() {
        return sendRequestAtMillis;
    }

    @Override
    public void setSendRequestAtMillis(long millis) {
        this.sendRequestAtMillis = millis;
    }

    @Override
    public long getReceivedResponseAtMillis() {
        return receivedResponseAtMillis;
    }

    @Override
    public void setReceivedResponseAtMillis(long millis) {
        this.receivedResponseAtMillis = millis;
    }
}
