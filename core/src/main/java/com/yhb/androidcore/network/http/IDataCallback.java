package com.yhb.androidcore.network.http;

/**
 * Created by yanghongbing on 16/12/31.
 */

public interface IDataCallback {

    void onDataReceived(IRequest request, IResponse response);

    boolean onDataError(IRequest request, IResponse response);
}
