package com.yhb.androidcore.network.http;

/**
 * Created by yanghongbing on 16/12/31.
 */

public interface IResponse {

    int getCode();
    void setCode(int code);
    String getMessage();
    void setMessage(String message);
    long getSendRequestAtMillis();
    void setSendRequestAtMillis(long millis);
    long getReceivedResponseAtMillis();
    void setReceivedResponseAtMillis(long millis);

}
