package com.yhb.androidcore.network.http;

import java.util.Map;

/**
 * Created by yanghongbing on 16/12/31.
 */

public interface IRequest {

    String getUrl();

    String getMethod();

    Map<String, String> getParams();

    Map<String, String> getHeaders();

    int getRequestId();

    void setMethod(String method);

    void setParams(Map<String, String> params);

    void addParam(String key, String value);

    void addParams(Map<String, String> params);

    void setHeaders(Map<String, String> headers);

}
