package com.yhb.androidcore.application;

import android.app.Application;

/**
 * Created by yanghongbing on 16/12/11.
 */

public class BaseApplication extends Application {

    private static BaseApplication instance;

    public static BaseApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
