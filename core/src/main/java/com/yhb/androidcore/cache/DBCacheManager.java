package com.yhb.androidcore.cache;



import com.yhb.androidcore.db.CacheDBUtils;

import java.util.List;

/**
 * Created by WANGLIANGBAI697 on 2016-02-23.
 */
public class DBCacheManager extends AbstractCacheManager {
    private CacheDBUtils dbUtils;

    public DBCacheManager() {
        super();
        this.dbUtils = new CacheDBUtils();
    }

    @Override
    public String getCache(String key) {
        return dbUtils.getCache(key);
    }

    @Override
    public void setCache(String key, Object value) {
        if (dbUtils.getCache(key) != null) {
            dbUtils.updateCache(key, (String) value);
        } else {
            dbUtils.insertCache(key, (String) value);
        }
    }

    @Override
    public void removeCache(String key) {
        dbUtils.deleteCache(key);
    }

    @Override
    public void clearCache() {
        dbUtils.deleteAllCache();
    }

    @Override
    public void clearUserCache() {
        List<String> keyList = dbUtils.getAllCacheKeys();
        if (keyList != null && !keyList.isEmpty()) {
            for (String key: keyList) {
                if (key.startsWith(CacheKeys.USER_PREFIX)) {
                    dbUtils.deleteCache(key);
                }
            }
        }
    }
}
