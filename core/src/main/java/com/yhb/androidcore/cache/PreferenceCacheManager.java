package com.yhb.androidcore.cache;



import com.yhb.androidcore.utils.SpUtils;
import com.yhb.androidcore.application.BaseApplication;

import java.util.Set;

/**
 * Created by YANGHONGBING615 on 2015/7/22.
 */
public class PreferenceCacheManager extends AbstractCacheManager {

    private final String CACHE_FILE_NAME = BaseApplication.getInstance().getPackageName() + "_cache";
    private SpUtils spUtils;

    public PreferenceCacheManager() {
        super();
        spUtils = new SpUtils(CACHE_FILE_NAME);
    }

    @Override
    public String getCache(String key) {
        return spUtils.getString(key, null);
    }

    @Override
    public void setCache(String key, Object value) {
        String valueStr = "";
        if (value != null) {
            valueStr = value.toString();
        }
        spUtils.setString(key, valueStr);
    }

    public void removeCache(String key) {
        if(getCache(key) != null) {
            spUtils.remove(key);
        }
    }

    public void clearCache() {
        spUtils.clear();
    }

    public void clearUserCache() {
        Set<String> keys = spUtils.getAllKeys();
        for (String key : keys) {
            if (key != null && key.startsWith(CacheKeys.USER_PREFIX)) {
                spUtils.remove(key);
            }
        }
    }
}
