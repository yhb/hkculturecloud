package com.yhb.androidcore.cache;

/**
 * Created by yanghongbing on 16/4/7.
 */
public interface OnCacheStatusChangedListener {
    void onCacheUpdate(int cacheMode, String cacheKey, Object value);
}
