package com.yhb.androidcore.cache;

/**
 * Created by YANGHONGBING615 on 2015/7/22.
 */
public interface CacheKeys {

    //跟用户相关的缓存前缀，如果缓存key加上此前缀，则用户在注销账号后，回删除此key对于的缓存
    String USER_PREFIX = "_USER_";

}
