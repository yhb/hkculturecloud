package com.yhb.androidcore.cache;

import android.app.ActivityManager;
import android.content.Context;
import android.support.v4.util.LruCache;

import com.yhb.androidcore.application.BaseApplication;

/**
 * Created by yanghongbing on 16/12/11.
 */

public class LruMemoryCache extends AbstractCacheManager {

    private LruCache<String, Object> lruCache;

    public LruMemoryCache() {
        super();
        final int memClass = ((ActivityManager) BaseApplication.getInstance().
                getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        lruCache = new LruCache<>(memClass);
    }

    @Override
    public Object getCache(String key) {
        return lruCache.get(key);
    }

    @Override
    public void setCache(String key, Object value) {
        lruCache.put(key, value);
    }

    @Override
    public void removeCache(String key) {
        lruCache.remove(key);
    }

    @Override
    public void clearCache() {
        lruCache.evictAll();
    }

    @Override
    public void clearUserCache() {
        //TODO:
    }
}
