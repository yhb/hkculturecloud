package com.yhb.androidcore.cache;

/**
 * Created by YANGHONGBING615 on 2015/7/22.
 */
public class CacheManager {

    public static final int MODE_MEMORY = 1;
    public static final int MODE_PREFERENCE = 2;
    public static final int MODE_DB = 4;
    public static final int MODE_ALL = MODE_MEMORY | MODE_PREFERENCE | MODE_DB;
    public static final int MODE_DEFAULT = MODE_MEMORY;

    private MemoryCacheManager memoryCacheManager;
    private PreferenceCacheManager preferenceCacheManager;
    private DBCacheManager dbCache;

    public CacheManager() {
        memoryCacheManager = new MemoryCacheManager();
        preferenceCacheManager = new PreferenceCacheManager();
        dbCache = new DBCacheManager();
    }

    public void setCache(String key, Object value) {
        setCache(MODE_DEFAULT, key, value);
    }

    public void setCache(int cacheMode, String key, Object value) {
        if((cacheMode & MODE_MEMORY) == MODE_MEMORY) {
            memoryCacheManager.setCache(key, value);
            memoryCacheManager.getCacheStatusHelper().notifySubscriber(MODE_MEMORY, key, value);
        }
        if((cacheMode & MODE_PREFERENCE) == MODE_PREFERENCE) {
            preferenceCacheManager.setCache(key, value);
            preferenceCacheManager.getCacheStatusHelper().notifySubscriber(MODE_PREFERENCE, key, value);
        }
        if ((cacheMode & MODE_DB) == MODE_DB) {
            dbCache.setCache(key, value);
            preferenceCacheManager.getCacheStatusHelper().notifySubscriber(MODE_DB, key, value);
        }
    }

    public Object getCache(String key) {
        return getCache(MODE_DEFAULT, key);
    }

    public Object getCache(int cacheMode, String key) {
        Object value = null;
        if((cacheMode & MODE_MEMORY) == MODE_MEMORY) {
            value = memoryCacheManager.getCache(key);
        }
        if((cacheMode & MODE_PREFERENCE) == MODE_PREFERENCE) {
            value = preferenceCacheManager.getCache(key);
        }
        if ((cacheMode & MODE_DB) == MODE_DB) {
            value = dbCache.getCache(key);
        }
        return value;
    }

    public void removeCache(String key) {
        removeCache(MODE_DEFAULT, key);
    }

    public void removeCache(int cacheMode, String key) {
        if((cacheMode & MODE_MEMORY) == MODE_MEMORY) {
            memoryCacheManager.removeCache(key);
            memoryCacheManager.getCacheStatusHelper().notifySubscriber(MODE_MEMORY, key, null);
        }
        if((cacheMode & MODE_PREFERENCE) == MODE_PREFERENCE) {
            preferenceCacheManager.removeCache(key);
            preferenceCacheManager.getCacheStatusHelper().notifySubscriber(MODE_PREFERENCE, key, null);
        }
        if ((cacheMode & MODE_DB) == MODE_DB) {
            dbCache.removeCache(key);
            dbCache.getCacheStatusHelper().notifySubscriber(MODE_DB, key, null);
        }
    }

    public void clear(int cacheMode) {
        if((cacheMode & MODE_MEMORY) == MODE_MEMORY) {
            memoryCacheManager.clearCache();
        }
        if((cacheMode & MODE_PREFERENCE) == MODE_PREFERENCE) {
            preferenceCacheManager.clearCache();
        }
        if ((cacheMode & MODE_DB) == MODE_DB) {
            dbCache.clearCache();
        }
    }

    public void clearUserCache() {
        memoryCacheManager.clearUserCache();
        preferenceCacheManager.clearUserCache();
        dbCache.clearUserCache();
    }

    public void clearAll() {
        memoryCacheManager.clearCache();
        preferenceCacheManager.clearCache();
        dbCache.clearCache();
    }

    public void setOnCacheStatusChangedListener(int cacheMode, String cacheKey, OnCacheStatusChangedListener listener) {
        if ((cacheMode & MODE_MEMORY) == MODE_MEMORY) {
            memoryCacheManager.getCacheStatusHelper().addListener(cacheKey, listener);
        }
        if ((cacheMode & MODE_PREFERENCE) == MODE_PREFERENCE) {
            preferenceCacheManager.getCacheStatusHelper().addListener(cacheKey, listener);
        }
        if ((cacheMode & MODE_DB) == MODE_DB) {
            dbCache.getCacheStatusHelper().addListener(cacheKey, listener);
        }
    }

    public void setOnCacheStatusChangedListener(String cacheKey, OnCacheStatusChangedListener onCacheStatusChangedListener) {
        setOnCacheStatusChangedListener(CacheManager.MODE_ALL, cacheKey, onCacheStatusChangedListener);
    }
}
