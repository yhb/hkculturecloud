package com.yhb.androidcore;

import android.test.AndroidTestCase;

import com.yhb.androidcore.network.http.HttpCallbackImpl;
import com.yhb.androidcore.network.http.HttpRequest;
import com.yhb.androidcore.network.http.HttpUtils;
import com.yhb.androidcore.network.http.IRequest;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Callback;

/**
 * Created by yanghongbing on 16/12/31.
 */

public class HttpUtilsTest extends AndroidTestCase {

    public void testHttpGet() {
        IRequest request = new HttpRequest(3042, "http://180.168.124.240:8080/hdkg/airways.json");
        Callback callback = new HttpCallbackImpl(null);
        HttpUtils.sendRequest(request, callback);
    }

//    public void testHttpPost() {
//        IRequest request = new HttpRequest(3018, "http://180.168.124.240:8080/hdkg/user/login.json");
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("imei", "865743021971071");
//        params.put("name", "yhb");
//        params.put("password", "111111");
//        request.setParams(params);
//        request.setMethod("POST");
//        Callback callback = new HttpCallbackImpl(null);
//        HttpUtils.sendRequest(request, callback);
//    }

    public void testHttpPostWithHeader() {
        IRequest request = new HttpRequest(3019, "http://180.168.124.240:8080/hdkg/flight/change_tobt.json");
        Map<String, String> params = new HashMap<String, String>();
        params.put("tobtReason", "飞机、保障车辆等待");
        params.put("tobt", "20170101000000");
        params.put("fuid", "9007fb28af3244738fa61b810916dfb1");
        request.setParams(params);
        request.setMethod("POST");
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMDUiLCJzdWIiOiJ5aGIiLCJpYXQiOjE0ODMyNTg0NjcsImV4cCI6MTQ4MzI1OTY2N30.GDi8VuBwX6pE4DThQ8n93kZDEDRzY-tkxhAbmNPiudQ");
        request.setHeaders(headers);
        Callback callback = new HttpCallbackImpl(null);
        HttpUtils.sendRequest(request, callback);
    }
}
